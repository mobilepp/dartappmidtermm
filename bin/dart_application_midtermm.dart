import 'package:dart_application_midtermm/dart_application_midtermm.dart'
    as dart_application_midtermm;

import 'dart:io';

List tokenizing(String ex) {
  var tokens00 = ex.replaceAll(' ', '');
  var tokens0 = tokens00.split('');
  //print(tokens0);
  var tokensl = new List<String>.filled(0, "m", growable: true);
  var nums = new List<String>.filled(0, "m", growable: true);
  var tokenJoin = new List<String>.filled(0, "m", growable: true);
  String join;

  for (var i = 0; i < tokens0.length; i++) {
    // tokensl.add(tokens0[i]);
    if (isParenthesis(tokens0[i]) == 1) {
      tokensl.add(tokens0[i]);
    }
    if (isInteger(tokens0[i])) {
      nums.add(tokens0[i]);
    }
    if (i == 0 && isOperator(tokens0[i])) {
      nums.add(tokens0[i]);
    } else if (i - 1 >= 0 &&
        isOperator(tokens0[i]) &&
        isOperator(tokens0[i - 1])) {
      nums.add(tokens0[i]);
    } else if (isOperator(tokens0[i])) {
      for (var n in nums) {
        tokenJoin.add(n);
      }
      nums.clear();
      join = tokenJoin.join();
      // print('$tokensl');
      tokensl.add(join);
      // print('$tokensl');
      if (i - 1 >= 0 && isParenthesis(tokens0[i - 1]) == 2) {
        tokensl.add(tokens0[i - 1]);
      }
      tokensl.add(tokens0[i]);
      // print('$tokensl');
      tokenJoin.clear();
    }
    if (i == tokens0.length - 1 && nums.isNotEmpty) {
      for (var n in nums) {
        tokenJoin.add(n);
      }
      nums.clear();
      join = tokenJoin.join();
      tokensl.add(join);
      tokenJoin.clear();
    }
    if (i == tokens0.length - 1 && isParenthesis(tokens0[i]) == 2) {
      tokensl.add(tokens0[i]);
    }
  }

  return tokensl;

  /*
  update!!! - 27/8/2022
  focus on operator "+ - * / % ^ ( )" and "Integer; I+, I0, I-" and all of pattern space then
  test : "5    +           3 + 1 - ( 2 - 3 ) % 2 - 6 ^ 4 % 2 - -25"
  should be : 5, +, 3, +, 1, -, (, 2, -, 3, ), %, 2, -, 6, ^, 4, %, 2, -, -25

  test : "5    +           3 + 1 -(2-3)%2-6^4%2"
  should be : 5, +, 3, +, 1, -, (, 2, -, 3, ), %, 2, -, 6, ^, 4, %, 2

  test : "5           +           3 + 1 -2-34%2-6^4%2"
  should be : 5, +, 3, +, 1, -, 2, -, 34, %, 2, -, 6, ^, 4, %, 2

  "1-222" -> 1,-,222
  "91+-111" -> 91,-111
  "(8+9)" -> (,8,+,9,)
  "4 +7* 6 - 10" -> 4, +, 7, *, 6, -, 10
  "( 5 + 4) / (4- 1)" -> (, 5, +, 4, ), /, (, 4, -, 1, )
  "10 ^ 2 ^ 3" -> 10, ^, 2, ^, 3 
  "10 -2 -3" -> 10, -, 2, -, 3
  "-2-3" -> -2,-,3
  */

  // for (var token in tokens0) {
  //   if (isInteger(token) || isOperator(token) || isParenthesis(token)) {
  //     //ignore white space
  //     tokensl.add(token);
  //   }
  // }
  // print(tokensl);

  /*
  update!!! - 24/8/2022
  focus on operator "+ - * / % ^ ( )" and "Integer; I+, I0, I-" and space then
  test : "5    +           3 + 1 - ( 2 - 3 ) % 2 - 6 ^ 4 % 2 - -25"
  should be : 5, +, 3, +, 1, -, (, 2, -, 3, ), %, 2, -, 6, ^, 4, %, 2, -, -25
  */

  //test : "5           +           3 + 1 -2-34%2-6^4%2"
  //should be : 5, +, 3, +, 1, -, 2, -, 34, %, 2, -, 6, ^, 4, %, 2

  /*  focus on operator "+ - * / % ^ ( )" and "number 0-9" then
  test : "5    +           3 + 1 -(2-3)%2-6^4%2"
  should be : 5, +, 3, +, 1, -, (, 2, -, 3, ), %, 2, -, 6, ^, 4, %, 2
  */
}

int isParenthesis(String token) {
  if (token.compareTo("(") == 0) {
    return 1;
  }
  if (token.compareTo(")") == 0) {
    return 2;
  }
  return 0;
}

bool isInteger(String str) {
  // for (var i = 0; i <= 9; i++) {
  try {
    if (int.tryParse(str) != null) {
      return true;
    }
  } on Exception catch (e) {}
  // }
  return false;
}

bool isOperator(String str) {
  for (String t in {"+", "-", "*", "/", "%", "^"}) {
    if (str.compareTo(t) == 0) {
      return true;
    }
  }
  return false;
}

int precedences(String str) {
  switch (str) {
    case "+":
    case "-":
      return 1;
    case "*":
    case "/":
    case "%":
      return 2;
    case "^":
      return 3;
    default:
      return 0;
  }
}

List infixToPostfix(List infix) {
  var oprList = new List<String>.filled(0, "m", growable: true);
  var postfix = new List<String>.filled(0, "m", growable: true);

  for (var token in infix) {
    if (isInteger(token)) {
      postfix.add(token);
    }
    if (isOperator(token)) {
      while (oprList.isNotEmpty &&
          (oprList.last.compareTo("(") != 0) &&
          (precedences(token) <= precedences(oprList.last))) {
        postfix.add(oprList.removeLast());
      }
      oprList.add(token);
    }
    if (token.compareTo("(") == 0) {
      oprList.add(token);
    }
    if (token.compareTo(")") == 0) {
      while (oprList.last.compareTo("(") != 0) {
        postfix.add(oprList.removeLast());
      }
      oprList.removeLast();
    }
  }
  while (oprList.isNotEmpty) {
    postfix.add(oprList.removeLast());
  }

/*
[5, +, 3, +, 1, -, (, 2, -, 3, ), %, 2, -, 6, ^, 4, %, 2]

symbol ========== oprList ========== postfix
5                   n                   5
+                   +                   5
3                   +                   53
+                   +                   53+
1                   +                   53+1
-                   -                   53+1+
(                   -(                  53+1+
2                   -(                  53+1+2
-                   -(-                 53+1+2
3                   -(-                 53+1+23
)                   -                   53+1+23-
%                   -%                  53+1+23-
2                   -%                  53+1+23-2
-                   -                   53+1+23-2%-
6                   -                   53+1+23-2%-6
^                   -^                  53+1+23-2%-6
4                   -^                  53+1+23-2%-64
%                   -%                  53+1+23-2%-64^
2                   -%                  53+1+23-2%-64^2
                                        53+1+23-2%-64^2%-
 */

  return postfix;
}

int applyOpr(int left, int right, String token) {
  var pow = 1;
  switch (token) {
    case "+":
      return left + right;
    case "-":
      return left - right;
    case "*":
      return left * right;
    case "/":
      return left ~/ right;
    case "%":
      return left % right;
    case "^":
      for (var i = 1; i <= right; i++) {
        pow *= left;
      }
      return pow;
    default:
      return 0;
  }
}

int evaluatePostfix(List postfix) {
  var values = new List<int>.filled(0, 0, growable: true);

  for (var token in postfix) {
    if (isInteger(token)) {
      var intToken = int.parse(token);
      values.add(intToken);
    } else {
      var right = values.removeLast();
      var left = values.removeLast();
      var result = applyOpr(left, right, token);
      values.add(result);
    }
  }

  /*
[5, 3, +, 1, +, 2, 3, -, 2, %, -, 6, 4, ^, 2, %, -]

          values
5           5
3           53
+           8
1           81
+           9
2           92
3           923
-           9-1
2           9-12
%           91
-           8
6           86
4           864
^           8(6^4)
2           8(6^4)2
%           80
-           8

 */

  return values.first;
}

void main() {
  String ex = stdin.readLineSync()!;
  var tokens = tokenizing(ex);
  print("Your list of tokens : \n$tokens");
  var postfix = infixToPostfix(tokens);
  print("Your list of postfix : \n$postfix");
  print("Your postfix :");
  print(postfix.join(",").replaceAll(",", ""));
  // for (String pf in postfix) {
  //   stdout.write(pf);
  // }
  var value = evaluatePostfix(postfix);
  print("Value : $value");
}
